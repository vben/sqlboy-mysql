import useDocusaurusContext from "@docusaurus/useDocusaurusContext";
import Layout from "@theme/Layout";
import React from "react";

function Home() {
  const context = useDocusaurusContext();
  const { siteConfig = {} } = context;
  return (
    <Layout
      title={`MySQL笔记 ${siteConfig.title}`}
      description="MySQL笔记"
    >
      <Banner />
    </Layout>
  );
}

function Banner() {
  return (
    <div>
      vbook
    </div>
  );
}


export default Home;
