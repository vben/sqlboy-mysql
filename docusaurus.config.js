module.exports = {
    title: "Vbook",
    tagline: "Vbook",
    url: "http://vbook.gitee.io/mysql/",
    baseUrl: "/mysql/",
    onBrokenLinks: "throw",
    onBrokenMarkdownLinks: "warn",
    favicon: "img/favicon.ico",
    organizationName: "Baiqian Co.,Ltd",
    projectName: "dbmd",
    // scripts: ["/vbook/script/baidutongji.js"],
    themeConfig: {
        autoCollapseSidebarCategories: true,
        prism: {
            additionalLanguages: ["powershell", "csharp", "sql"],
            // theme: require("prism-react-renderer/themes/github"),
            // darkTheme: require("prism-react-renderer/themes/dracula"),
        },
        algolia: {
            // appId: "XYY4NGVXSA",
            // apiKey: "957b35892d68e9ac86c35c96d89dcedf",
            // indexName: "furion",
            // contextualSearch: true,
            appId: "IP979HX57W",
            apiKey: "fca82ddffae79c4a7142c6cf84ad298d",
            // apiKey: "47d0c1f82d921b752a5c8915b90cac02",
            indexName: "mysql",
            contextualSearch: true,
        },
        navbar: {
            title: "MySQL笔记",
            logo: {
                alt: "vbook Logo",
                src: "img/vbook.png",
            },
            hideOnScroll: true,
            items: [
                {
                    to: "docs/dev/01/dev_intro",
                    activeBasePath: "docs/dev",
                    label: "DEV文档",
                    position: "left",
                },
                {
                    to: "docs/dba/01/dba_intro",
                    activeBasePath: "docs/dba",
                    label: "DBA文档",
                    position: "left",
                },
                {
                    to: "docs/test/sql_intro",
                    activeBasePath: "docs/test",
                    label: "练习",
                    position: "left",
                },
                {
                    label: "Gitee",
                    position: "right",
                    href: "https://gitee.com/vbook/vbook-mysql",
                },
            ],
        },
        footer: {
            style: "light",
            copyright: `Copyright © ${
                new Date().getFullYear() - 1
            }-${new Date().getFullYear()} vbook`,
        },
    },
    presets: [
        [
            "@docusaurus/preset-classic",
            {
                docs: {
                    sidebarPath: require.resolve("./sidebars.js"),
                    editUrl: "https://gitee.com/vbook/vbook-mysql/blob/master",
                    showLastUpdateTime: true,
                    showLastUpdateAuthor: true,
                    // sidebarCollapsible: true,
                },
                // blog: {
                //     showReadingTime: true,
                //     editUrl: "https://gitee.com/dotnetchina/Furion/tree/net6/handbook/",
                // },
                theme: {
                    customCss: require.resolve("./src/css/custom.css"),
                },
            },
        ],
    ],
};
