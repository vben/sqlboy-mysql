module.exports = {
  dev: [
    {
      type: "category",
      label: "1. 序言",
      items: [
        "dev/01/dev_intro",
        "dev/01/dev_ready",
      ],
    },
    {
      type: "category",
      label: "2. 数据查询 DQL",
      items: [
        "dev/02/base_query",
        "dev/02/operator",
        "dev/02/mu_query",
        "dev/02/single_fun",
        "dev/02/aggre_fun",
        "dev/02/sub_query",
      ],
    },
    {
      type: "category",
      label: "3. 数据操纵 DML",
      items: [
        "dev/03/insert",
        "dev/03/update",
        "dev/03/delete",
      ],
    },
    {
      type: "category",
      label: "4. 数据定义 DDL",
      items: [
        "dev/04/ddl_base",
        "dev/04/database",
        "dev/04/table",
        "dev/04/data_type",
        "dev/04/constraint",
        "dev/04/view",
        "dev/04/procedure",
        "dev/04/process_control",
        "dev/04/trigger",
      ],
    },
    {
      type: "category",
      label: "5. 事务控制 TCL",
      items: [
        "dev/05/eee",
        "dev/05/fff",
      ],
    },
  ],
  dba: [
    {
      type: "category",
      label: "1. 序言",
      items: [
        "dba/01/dba_intro",
        "dba/01/dba_ready",
      ],
    },
    {
      type: "category",
      label: "2. 环境搭建",
      items: [
        "dba/02/winstall",
        "dba/02/wstartup",
        "dba/02/mtools",
      ],
    },
    {
      type: "category",
      label: "3. 数据控制 DDL",
      items: [
        "dba/03/uuu",
        "dba/03/vvv",
      ],
    },
    {
      type: "category",
      label: "4. 数据库日志",
      items: [
        "dba/04/www",
        "dba/04/xxx",
      ],
    },
    {
      type: "category",
      label: "5. 备份与恢复",
      items: [
        "dba/05/yyy",
        "dba/05/zzz",
      ],
    },
  ],
  test: [
    {
      type: "category",
      label: "1. 序言",
      items: [
        "test/sql_intro",
        "test/sql_rule",
      ],
    },
  ]
};
